"""Determine the correct kpet tree for a tree family and the kernel NVR."""
import argparse
import re
import sys

mapping = {
    r'rhel5': [
        (r'\.el5', 'rhel5-els'),                                 # extended life cycle
    ],
    r'rhel6': [
        (r'\.el6', 'rhel610-z'),                                 # supported zstream 6.10
    ],
    r'rhel7': [
        (r'3\.10\.0-327\b.*\.el7', 'rhel72-z'),                  # supported zstream 7.2
        (r'3\.10\.0-514\b.*\.el7', 'rhel73-z'),                  # supported zstream 7.3
        (r'3\.10\.0-693\b.*\.el7', 'rhel74-z'),                  # supported zstream 7.4
        (r'3\.10\.0-957\b.*\.el7', 'rhel76-z'),                  # supported zstream 7.6
        (r'3\.10\.0-1062\b.*\.el7', 'rhel77-z'),                 # supported zstream 7.7
        (r'3\.10\.0-1160\b.*\.el7', 'rhel79-z'),                 # supported zstream 7.9
    ],
    r'rhel7-rt': [
        (r'3\.10\.0-957\b.*\.el7', 'rhel76-z-rt'),               # supported zstream 7.6
        (r'3\.10\.0-1160\b.*\.el7', 'rhel79-z-rt'),              # supported zstream 7.9
    ],
    r'rhel8(?P<suffix>-rt|)': [
        (r'\.el8_(?P<minor>[124])', 'rhel8{minor}-z{suffix}'),   # supported zstream 8.[124]
        (r'\.el8_[56789]', 'rhel8{suffix}'),                  # unrecognized new zstream -> ystream
        (r'\.el8(?!_)', 'rhel8{suffix}'),                     # ystream
    ],
    r'rhel9(?P<suffix>-rt|)': [
        (r'\.el9_\d+', 'rhel9{suffix}'),                      # unrecognized new zstream -> ystream
        (r'\.el9(?!_)', 'rhel9{suffix}')                      # ystream
    ],
}


def map_nvr(tree_family, nvr):
    """Return the kpet tree name for a tree family and an NVR."""
    for family_regex, nvr_regexes in mapping.items():
        family_match = re.fullmatch(family_regex, tree_family)
        if not family_match:
            continue
        for nvr_regex, tree_name in nvr_regexes:
            nvr_regex = nvr_regex.format_map(family_match.groupdict())
            nvr_match = re.search(nvr_regex, nvr)
            if nvr_match:
                return tree_name.format_map(dict(**family_match.groupdict(),
                                                 **nvr_match.groupdict()))

        # Raise exception here, so we can fixup tree mapping and restart the pipeline.
        raise RuntimeError('kpet tree name for this NVR could not be matched')
    return tree_family


def main(args):
    """Run the main command line interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument('tree_family', help='kpet tree family')
    parser.add_argument('nvr', help='kernel Name-Version-Release information')
    parsed_args = parser.parse_args(args)

    print(map_nvr(parsed_args.tree_family, parsed_args.nvr))


if __name__ == '__main__':
    main(sys.argv[1:])
