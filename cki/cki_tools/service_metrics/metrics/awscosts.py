"""AWS cost metrics."""
import os

from cki_lib.logger import get_logger
import prometheus_client
import yaml

from cki.monitoring_tools import aws_cost_explorer

from .. import base

LOGGER = get_logger(__name__)
AWSCOSTS_CONFIG = yaml.safe_load(os.environ.get('AWSCOSTS_CONFIG', '{}'))


class AwsCostMetrics(base.Metric):
    """Calculate AWS cost metrics."""

    # once a day slightly after UTC midnight which should return the data of the previous UTC day
    schedule = '10 0 * * *'

    metric_cost = prometheus_client.Gauge(
        'cki_aws_cost',
        'grouped AWS costs',
        ['type', 'key']
    )

    def update(self, **_):
        """Update the bucket metrics."""
        filters = AWSCOSTS_CONFIG.get('filters', {})
        explorer = aws_cost_explorer.AwsCostExplorer(days=1, filters=filters)
        for cost_type, cost_group in aws_cost_explorer.Cost.__members__.items():
            if cost_type == 'DAILY':
                continue
            for cost in explorer.costs(cost_group):
                self.metric_cost.labels(cost_type.lower(), cost[0]).set(cost[1])
