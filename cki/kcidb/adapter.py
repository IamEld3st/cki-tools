"""Convert pipeline rc-file to kcidb data without touching the pipeline."""
import argparse
import os
import pathlib
from urllib.parse import urlparse

from cached_property import cached_property
from cki_lib.kcidb.file import KCIDBFile
from cki_lib.logger import get_logger
from cki_lib.misc import get_env_int
from cki_lib.misc import strtobool
from cki_lib.session import get_session
from dateutil.parser import parse as date_parse
from rcdefinition.rc_data import SKTData
from rcdefinition.trigger_variables import TriggerVariables

from cki.cki_tools.get_kernel_headers import KojiWrap
from cki.kcidb import utils

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)


class KCIDBAdapter:
    """Convert pipeline data into kcidb data."""

    def __init__(self, obj_id, kcidb_file_path, upload=True):
        """Create object."""
        self.obj_id = obj_id
        self.kcidb_file = KCIDBFile(kcidb_file_path)
        self.upload = upload

        rcfile_path = pathlib.Path(os.environ['CI_PROJECT_DIR'], 'rc')
        self.rc_data = SKTData.deserialize(
            rcfile_path.read_text(encoding='utf8') if rcfile_path.is_file() else None
        )

        self.visibility = (
            'public' if os.environ.get('kernel_type', 'internal') == 'upstream' else 'private'
        )

        # Relative path where to put the files in the storage.
        self.artifacts_path_prefix = f"{os.environ['CI_JOB_YMD']}/{os.environ['CI_PIPELINE_ID']}"

    def upload_file(self, artifacts_path, file_name, file_content=None,
                    source_path=None):
        """Upload a file, if upload is allowed."""
        if not self.upload:
            LOGGER.info('uploading files is disabled')
            return None

        return utils.upload_file(self.visibility, artifacts_path, file_name,
                                 file_content=file_content, source_path=source_path)

    def generate(self):
        # pylint: disable=no-self-use
        """Populate data fields. Needs to be overriden on each class."""
        raise NotImplementedError

    def dump(self):
        # pylint: disable=no-self-use
        """Return the generated data. Needs to be overriden on each class."""
        raise NotImplementedError

    def update(self):
        """Return KCIDBFile updated with the new objects."""
        self.generate()
        kcidb_objects = self.dump()

        for checkout in kcidb_objects.get('checkouts', []):
            self.kcidb_file.get_checkout(checkout['id']).update(checkout)

        for build in kcidb_objects.get('builds', []):
            self.kcidb_file.get_build(build['id']).update(build)

        # KCIDBFile does not support tests for now, add it directly to the data.
        tests = kcidb_objects.get('tests')
        if tests:
            self.kcidb_file.data.setdefault('tests', [])
            self.kcidb_file.data['tests'].extend(tests)

        return self.kcidb_file

    @staticmethod
    def gitlab_data():
        """Return 'job' and 'pipeline' dicts for misc field."""
        variables = TriggerVariables().pipeline_vars_from_env()
        return {
            'job': {
                'id': get_env_int('CI_JOB_ID', 0),
            },
            'pipeline': {
                'id': get_env_int('CI_PIPELINE_ID', 0),
                'variables': variables,
                'ref': os.environ['CI_COMMIT_REF_NAME'],
                'project': {
                    'id': get_env_int('CI_PROJECT_ID', 0),
                    'path_with_namespace': os.environ['CI_PROJECT_PATH'],
                    'instance_url': os.environ['CI_SERVER_URL']
                }
            }
        }

    @staticmethod
    def remove_empty_values(obj):
        """Remove keys with empty value from a dictionary."""
        return {k: v for k, v in obj.items() if not utils.is_empty(v)}


class Checkout(KCIDBAdapter):
    """Generate KCIDB Checkout from current job."""

    def __init__(self, *args, **kwargs):
        """Initialize."""
        super().__init__(*args, **kwargs)
        self.checkout = self.kcidb_file.get_checkout(self.obj_id)

    def generate(self):
        """
        Construct KCIDB Checkout.

        Update the checkout from kcidb_file adding extra data
        not populated in the job.
        """
        patchset_files = [
            {
                'url': url,
                'name': pathlib.Path(urlparse(url).path).name  # ignores trailing slashes
            } for url in os.environ.get('patch_urls', '').split()
        ]

        data = {
            'tree_name': os.environ['name'],
            'git_repository_url': os.environ['git_url'],
            'git_commit_hash': os.environ['commit_hash'],
            'git_repository_branch': os.environ['branch'],
            'patchset_files': patchset_files,
            'patchset_hash': self.patchset_hash,
            'message_id': os.environ.get('message_id'),
            'misc': self.gitlab_data(),
            'log_url': self.log_url,
        }

        self.checkout.update(self.remove_empty_values(data))

    def dump(self):
        """Return object's data."""
        return {'checkouts': [self.checkout]}

    @property
    def patchset_hash(self):
        """Return patchset_hash value."""
        return utils.patch_list_hash(
            os.environ.get('patch_urls', '').split()
        )

    @property
    def artifacts_path(self):
        """Relative path to checkout artifacts."""
        return f'{self.artifacts_path_prefix}/{self.checkout["id"]}'

    @property
    def log_url(self):
        """Return the URL of the log file of the attempt to construct checkout."""
        if self.rc_data.state.mergelog and self.artifacts_path:
            if not pathlib.Path(self.rc_data.state.mergelog).exists():
                # Sometimes the mergelog is defined but the file is missing
                return None

            return self.upload_file(self.artifacts_path, 'merge.log',
                                    source_path=self.rc_data.state.mergelog)
        return None


class Build(KCIDBAdapter):
    """Generate KCIDB Build from current job."""

    def __init__(self, *args, **kwargs):
        """Initialize."""
        super().__init__(*args, **kwargs)
        self.build = self.kcidb_file.get_build(self.obj_id)
        self.checkout = self.kcidb_file.get_checkout(self.build['checkout_id'])

    def generate(self):
        """Construct KCIDB Build and selftests."""
        # The checkout_id from rc file is used to identify the build, if it's
        # available, otherwise it's computed.
        comment = f'CKI build of {os.environ["name"]}'
        if self.rc_data.state.patch_subjects:
            comment += f' with {self.rc_data.state.patch_subjects[0]} patches'

        data = {
            'comment': comment,
            'config_url': self.config_url,
            'output_files': self.output_files,
            'config_name': os.environ['config_target'],
            'misc': self.gitlab_data(),
            'log_url': self.log_url,
        }

        self.build.update(self.remove_empty_values(data))

    def dump(self):
        """Return object's data."""
        return {'builds': [self.build], 'tests': self.selftests}

    @property
    def artifacts_path(self):
        """Get directory of artifacts for this build."""
        return (
            f'{self.artifacts_path_prefix}/' +
            f'{self.checkout["id"]}/{self.build["id"]}'
        )

    @property
    def config_url(self):
        """Return the URL of the build configuration file."""
        if self.rc_data.state.config_file and self.artifacts_path:
            return self.upload_file(self.artifacts_path, '.config',
                                    source_path=self.rc_data.state.config_file)
        return None

    @property
    def output_files(self):
        """Construct urls to build output files and upload them."""
        files = []
        state = self.rc_data.state
        for fpath in (state.tarball_file, state.selftests_file,
                      state.selftests_buildlog):
            if fpath:
                fpath_obj = pathlib.Path(fpath)
                if not fpath_obj.is_file():
                    LOGGER.error("Job %s: File not found: %s", os.environ['CI_JOB_ID'],
                                 fpath)

                    continue
                file_name = fpath_obj.name
                url = self.upload_file(self.artifacts_path, file_name,
                                       source_path=str(fpath_obj))
                if url:
                    output_file = {'name': file_name, 'url': url}
                    files.append(output_file)
                    if fpath == state.selftests_buildlog:
                        for selftest in self.selftests:
                            selftest.setdefault('output_files', [])
                            selftest['output_files'].append(output_file)

        if self.rc_data.state.repo_path:
            files.append({'name': 'kernel_package_url',
                          'url': self.rc_data.state.kernel_package_url})
        return files

    @property
    def log_url(self):
        """Return the URL of the build log file."""
        if self.rc_data.state.buildlog and self.artifacts_path:
            return self.upload_file(self.artifacts_path, 'build.log',
                                    source_path=self.rc_data.state.buildlog)
        return None

    @cached_property
    def selftests(self):
        """Generate KCIDB Tests for selftests if present."""
        if not os.environ.get('CI_JOB_STAGE') == 'build':
            # Only create selftests in the build stage to avoid repeated objects.
            return []
        selftests = []
        selftests_build_results_path = pathlib.Path(
            os.environ.get('SELFTESTS_BUILD_RESULTS_PATH', '')
        )
        if selftests_build_results_path.is_file():
            selftest_data = selftests_build_results_path.read_text(encoding='utf8')
            for test_info in utils.parse_selftests(selftest_data, self.build['architecture']):
                test = {
                    'build_id': self.build['id'],
                    'origin': 'redhat',
                    'id': f'{self.build["id"]}_{test_info.get("test_index")}',
                    'path': test_info.get('CKI_UNIVERSAL_ID'),
                    'comment': test_info.get('CKI_NAME'),
                    'status': "PASS" if test_info['return_code'] == 0 else "FAIL",
                    'waived': False,
                    'misc': self.gitlab_data()
                }
                selftests.append(self.remove_empty_values(test))

        return selftests


class BrewCheckout(KCIDBAdapter):
    """Generate KCIDB Checkout from current Brew/COPR job."""

    def __init__(self, *args, **kwargs):
        """Initialize."""
        super().__init__(*args, **kwargs)
        self.checkout = self.kcidb_file.get_checkout(self.obj_id)

    def generate(self):
        """Return checkout object populated with information about current brew checkout."""
        contact = os.environ.get('submitter')
        data = {
            'valid': True,
            'origin': 'redhat',
            'contacts': [contact] if contact else [],
            'tree_name': os.environ['name'],
            'misc': self.gitlab_data()
        }
        self.checkout.update(self.remove_empty_values(data))

    def dump(self):
        """Return object's data."""
        return {'checkouts': [self.checkout]}


class BrewBuild(KCIDBAdapter):
    """Generate KCIDB Build from current Brew/COPR job."""

    def __init__(self, *args, **kwargs):
        """Initialize."""
        super().__init__(*args, **kwargs)
        self.build = self.kcidb_file.get_build(self.obj_id)

    def generate(self):
        """Construct KCIDB Build for brew build."""
        data = {
            'origin': 'redhat',
            'duration': self.duration,
            'valid': True,
            'misc': {
                'debug': strtobool(self.rc_data.state.debug_kernel or 'false'),
                **self.gitlab_data()
            }
        }
        self.build.update(self.remove_empty_values(data))

    def dump(self):
        """Return object's data."""
        return {'builds': [self.build]}

    @cached_property
    def brew_task(self):
        # pylint: disable=no-self-use
        """Get brew task info."""
        koji = KojiWrap(os.environ['web_url'], os.environ['server_url'],
                        os.environ['top_url'])
        return koji.taskinfo(os.environ['brew_task_id'])

    @cached_property
    def copr_task(self):
        """Get COPR task info."""
        if not os.environ.get('copr_build'):
            # Not a COPR task
            return None

        build_id = os.environ['copr_build']
        name = os.environ['name']
        arch = self.build['architecture']

        copr_server = os.environ.get('COPR_SERVER_URL')
        url = f'{copr_server}/api_2/build_tasks/{build_id}/{name}-{arch}'
        return SESSION.get(url).json()['build_task']

    @property
    def duration(self):
        """Build duration."""
        if os.environ.get('brew_task_id'):
            return (date_parse(self.brew_task['Finished']) -
                    date_parse(self.brew_task['Started'])).total_seconds()

        return self.copr_task['ended_on'] - self.copr_task['started_on']


def main(argv=None):
    """Convert and dump data."""
    parser = argparse.ArgumentParser(
        description='Dump pipeline data as kcidb json.')
    parser.add_argument('stage', choices=['build', 'checkout'], help='What kind of object to dump.')
    parser.add_argument('id', help='ID of the KCIDB object.')
    parser.add_argument('--no-upload', dest='upload', action='store_false',
                        help="By default, files are uploaded. "
                             "Use --no-upload to prevent this behavior.")
    args = parser.parse_args(argv)

    generators = {
        'cki': {
            'checkout': Checkout,
            'build': Build,
        },
        'brew': {
            'checkout': BrewCheckout,
            'build': BrewBuild,
        },
    }

    kcidb_all_path = pathlib.Path(os.environ['KCIDB_DUMPFILE_NAME'])
    is_brew_or_copr = bool(
        os.environ.get('brew_task_id') or
        os.environ.get('copr_build')
    )
    pipeline_type = 'brew' if is_brew_or_copr else 'cki'

    generator = generators[pipeline_type][args.stage]
    kcidb_all = generator(args.id, kcidb_all_path, args.upload).update()
    kcidb_all.save()


if __name__ == '__main__':
    main()
