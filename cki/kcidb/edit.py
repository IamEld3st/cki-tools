#!/usr/bin/env python3
"""Shell interface for pipeline kcidb data."""
import argparse
import sys

from cki_lib import misc
from cki_lib.kcidb.file import KCIDBFile

from cki.kcidb import utils

NESTED_KEYS = {
    'misc-tag': 'misc/tag',
    'misc-test-hash': 'misc/test_hash',
    'misc-commit-message-title': 'misc/commit_message_title',
    'misc-kernel-version': 'misc/kernel_version',
    'misc-debug': 'misc/debug',
    'misc-targeted-tests': 'misc/targeted_tests',
}


def parse_time(value):
    """
    Parse time value and return it formatted.

    'now' value is turned into utc_now_iso(), anything else is
    converted from string to utc timestamp.
    """
    if value == 'now':
        return misc.utc_now_iso()

    return utils.tzaware_str2utc(value)


def _get(kcidb_file, args):
    getter = getattr(kcidb_file, f'get_{args.toplevel_object}')
    return getter(args.id)


def kcidb_set(kcidb_file, args):
    """Set key=value in toplevel_object."""
    if args.key in NESTED_KEYS:
        args.key = NESTED_KEYS[args.key]

    obj = _get(kcidb_file, args)
    misc.set_nested_key(obj, args.key, args.value)
    kcidb_file.save()


def kcidb_get(kcidb_file, args):
    """Print value for key from toplevel_object."""
    if args.key in NESTED_KEYS:
        args.key = NESTED_KEYS[args.key]

    obj = _get(kcidb_file, args)
    print(misc.get_nested_key(obj, args.key))


def create_checkout(kcidb_file, args):
    """Create checkout."""
    kcidb_file.set_checkout(
        args.id,
        {
            'id': args.id,
            'origin': 'redhat',
        }
    )
    kcidb_file.save()


def create_build(kcidb_file, args):
    """Create build."""
    kcidb_file.set_build(
        args.id,
        {
            'id': args.id,
            'origin': 'redhat',
            'checkout_id': args.checkout_id,
        }
    )
    kcidb_file.save()


def create(kcidb_file, args):
    """Create checkout or build."""
    if args.toplevel_object == 'checkout':
        create_checkout(kcidb_file, args)
    elif args.toplevel_object == 'build':
        create_build(kcidb_file, args)


def parse_args(argv):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description='Edit a KCIDB file.'
    )

    parser.add_argument(
        'kcidb_file',
        type=str,
        help='A path to the KCIDB file to read/write.',
    )

    parser.add_argument(
        'toplevel_object',
        choices=['checkout', 'build'],
        help='Which toplevel KCIDB object to operate on.',
    )
    parser.add_argument('id', type=str, help='KCIDB object id')

    subparsers = parser.add_subparsers(dest='action')
    subparsers.required = True  # py<3.7 compat

    # Get parser
    parser_get = subparsers.add_parser('get')
    parser_get.set_defaults(func=kcidb_get)
    parser_get.add_argument('key', type=str)

    # Set parsers
    setters = [
        ('', str),
        ('-bool', misc.strtobool),
        ('-int', int),
        ('-time', parse_time),
    ]

    for name, value_type in setters:
        subparser = subparsers.add_parser(f'set{name}')
        subparser.set_defaults(func=kcidb_set)
        subparser.add_argument('key', type=str)
        subparser.add_argument('value', type=value_type)

    parser_create = subparsers.add_parser('create')
    parser_create.set_defaults(func=create)
    parser_create.add_argument(
        'checkout_id',
        type=str,
        nargs='?',
        default=None
    )

    return parser.parse_args(argv)


def main(argv):
    """Open file and run appropriate function."""
    args = parse_args(argv)

    kcidb_file = KCIDBFile(args.kcidb_file)

    args.func(kcidb_file, args)


if __name__ == '__main__':
    main(sys.argv[1:])
