import datetime
import json
import os
import unittest
from unittest import mock

from freezegun import freeze_time
import responses
import urllib3_mock
import yaml

from cki.cki_tools import service_metrics
from cki.cki_tools.service_metrics import base
from cki.cki_tools.service_metrics import metrics
from cki.cki_tools.service_metrics.metrics.sentry import EventsCount
from cki.cki_tools.service_metrics.metrics.sentry import SentryAPI

URLLIB3_MOCK = urllib3_mock.Responses('requests.packages.urllib3')

BEAKER_CONFIG = yaml.safe_load("""
---
beaker_url: https://beaker.com

pools:
  - pool-1
  - pool-2

queues:
  aarch64:
  x86_64:
  i386: [i386, x86_64]
  ppc64:
  ppc64le:

systems:
  - system-1
  - system-2
""")

S3_CONFIG = yaml.safe_load("""
---
buckets:
  - description: bucket 1
    name: BUCKET_1
  - description: bucket 2
    name: BUCKET_2
""")

SENTRY_INSTANCES = yaml.safe_load("""
- url: https://sentry.io
  token_name: SENTRY_IO_TOKEN
  org: red-hat
  team: cki
""")


class TestGeneral(unittest.TestCase):
    """Tests not grouped in other classes."""

    def test_all_metrics(self):
        """Test ALL_METRICS has all the expected classes."""
        all_metrics = [
            metrics.AwsCostMetrics,
            metrics.BeakerMetrics,
            metrics.DatawarehouseSQLMetrics,
            metrics.S3BucketMetrics,
            metrics.TEIIDBeakerMetrics,
            metrics.SentryMetrics,
            metrics.KubernetesMetrics,
            metrics.VolumeMetrics,
        ]
        self.assertEqual(
            len(metrics.ALL_METRICS), len(all_metrics)
        )

        # Check all metrics from all_metrics are in metrics.ALL_METRICS
        for metric in all_metrics:
            self.assertTrue(
                any(m == metric for m in metrics.ALL_METRICS)
            )

    def test_get_enabled_metrics(self):
        """Test get_enabled_metrics detects the enabled metrics correctly."""
        service_metrics.METRICS_CONFIG = {}
        self.assertEqual([], service_metrics.get_enabled_metrics())

        service_metrics.METRICS_CONFIG = {'volumemetrics_enabled': True}
        self.assertEqual(
            set([metrics.VolumeMetrics]),
            set(m.__class__ for m in service_metrics.get_enabled_metrics()),
        )

        service_metrics.METRICS_CONFIG = {
            'volumemetrics_enabled': True,
            's3bucketmetrics_enabled': True,
            'sentrymetrics_enabled': False,
        }
        self.assertEqual(
            set([metrics.VolumeMetrics, metrics.S3BucketMetrics]),
            set(m.__class__ for m in service_metrics.get_enabled_metrics()),
        )

    def test_get_enabled_metrics_default_enabled(self):
        """Test get_enabled_metrics with default_enabled."""
        service_metrics.METRICS_CONFIG = {'default_enabled': False}
        self.assertEqual([], service_metrics.get_enabled_metrics())

        service_metrics.METRICS_CONFIG = {'default_enabled': True}
        self.assertEqual(
            set(metrics.ALL_METRICS),
            set(m.__class__ for m in service_metrics.get_enabled_metrics()),
        )

        service_metrics.METRICS_CONFIG = {
            'default_enabled': True,
            'kubernetesmetrics_enabled': False,
            'volumemetrics_enabled': False,
        }
        self.assertEqual(
            set(metrics.ALL_METRICS) - {metrics.KubernetesMetrics, metrics.VolumeMetrics},
            set(m.__class__ for m in service_metrics.get_enabled_metrics()),
        )

        service_metrics.METRICS_CONFIG = {
            'default_enabled': False,
            'kubernetesmetrics_enabled': True,
            'volumemetrics_enabled': True,
        }
        self.assertEqual(
            {metrics.KubernetesMetrics, metrics.VolumeMetrics},
            set(m.__class__ for m in service_metrics.get_enabled_metrics()),
        )

    def test_crontab(self):
        """Test crontab processing."""
        metric = base.Metric()
        metric.update = mock.Mock()
        metric.schedule = '0 1,2 * * *'
        with freeze_time('2000-01-01T00:00:00', tz_offset=-2) as frozen_time:
            # initial run
            metric.check_and_update()
            self.assertEqual(metric.update.mock_calls, [mock.call(last_run_datetime=None)])
            metric.update.mock_calls = []

            # not run again if initial run happened
            metric.check_and_update()
            self.assertEqual(metric.update.mock_calls, [])

            # not run again if schedule did not expire
            frozen_time.tick(delta=datetime.timedelta(minutes=30))
            metric.check_and_update()
            self.assertEqual(metric.update.mock_calls, [])

            # run if schedule expires
            frozen_time.tick(delta=datetime.timedelta(minutes=60))
            metric.check_and_update()
            self.assertEqual(metric.update.mock_calls, [
                mock.call(last_run_datetime=datetime.datetime.fromisoformat(
                    '2000-01-01T00:00:00').replace(tzinfo=datetime.timezone.utc)),
            ])
            metric.update.mock_calls = []

            # run again if schedule expires again
            frozen_time.tick(delta=datetime.timedelta(minutes=60))
            metric.check_and_update()
            self.assertEqual(metric.update.mock_calls, [
                mock.call(last_run_datetime=datetime.datetime.fromisoformat(
                    '2000-01-01T01:30:00').replace(tzinfo=datetime.timezone.utc)),
            ])
            metric.update.mock_calls = []


class TestBeakerMetrics(unittest.TestCase):
    """Test BeakerMetrics methods."""

    @staticmethod
    @mock.patch('cki.cki_tools.service_metrics.metrics.beaker.BEAKER_CONFIG', BEAKER_CONFIG)
    @responses.activate
    def test_update_pool_count():
        """Test update_pool_count method."""
        responses.add(
            responses.GET, 'https://beaker.com/pools/pool-1',
            json={'systems': ['system-1', 'system-2', 'system-3']}
        )
        responses.add(
            responses.GET, 'https://beaker.com/pools/pool-2',
            json={'systems': ['system-4']}
        )
        beaker_metrics = metrics.BeakerMetrics()

        beaker_metrics.metric_pool_count = mock.Mock()
        beaker_metrics.update_pool_count()

        beaker_metrics.metric_pool_count.assert_has_calls([
            mock.call.labels('pool-1'),
            mock.call.labels().set(3),
            mock.call.labels('pool-2'),
            mock.call.labels().set(1),
        ])

    @staticmethod
    @mock.patch('cki.cki_tools.service_metrics.metrics.beaker.BEAKER_CONFIG', BEAKER_CONFIG)
    @responses.activate
    def test_update_system():
        """Test update_system method."""
        responses.add(
            responses.GET, 'https://beaker.com/systems/system-1',
            json={
                'status': 'Automated',
                'current_reservation': {'recipe_id': 1234},
                'current_loan': {'recipient': 'some-team'},
            }
        )
        responses.add(
            responses.GET, 'https://beaker.com/systems/system-2',
            json={'status': 'Automated', 'current_reservation': None}
        )
        beaker_metrics = metrics.BeakerMetrics()

        beaker_metrics.metric_system = mock.Mock()
        beaker_metrics.update_system()

        beaker_metrics.metric_system.assert_has_calls([
            mock.call.labels('system-1'),
            mock.call.labels().info(
                {'status': 'Automated', 'recipe_id': '1234', 'loaned_to': 'some-team'}
            ),
            mock.call.labels('system-2'),
            mock.call.labels().info(
                {'status': 'Automated', 'recipe_id': '0', 'loaned_to': ''}
            ),
        ])

    def test_update(self):
        """Test update calls all the methods."""
        beaker_metrics = metrics.BeakerMetrics()

        beaker_metrics.update_system = mock.Mock()
        beaker_metrics.update_pool_count = mock.Mock()
        beaker_metrics.update()

        self.assertTrue(beaker_metrics.update_system.called)
        self.assertTrue(beaker_metrics.update_pool_count.called)


class TestDatawarehouseSQLMetrics(unittest.TestCase):
    """Test DatawarehouseSQLMetrics methods."""

    @staticmethod
    def test_update_time_to_build():
        """Test update_time_to_build method."""
        sql_metrics = metrics.DatawarehouseSQLMetrics()

        sql_metrics.metric_time_to_build = mock.Mock()
        sql_metrics.db_handler = mock.Mock()
        sql_metrics.db_handler.execute.return_value = [[1], [2]]

        sql_metrics.update_time_to_build(datetime.datetime.utcnow())
        sql_metrics.metric_time_to_build.assert_has_calls([
            mock.call.observe(1),
            mock.call.observe(2),
        ])

    @staticmethod
    def test_update_time_to_report():
        """Test update_time_to_report method."""
        sql_metrics = metrics.DatawarehouseSQLMetrics()

        sql_metrics.metric_time_to_report = mock.Mock()
        sql_metrics.db_handler = mock.Mock()
        sql_metrics.db_handler.execute.return_value = [[1], [2]]

        sql_metrics.update_time_to_report(datetime.datetime.utcnow())
        sql_metrics.metric_time_to_report.assert_has_calls([
            mock.call.observe(1),
            mock.call.observe(2),
        ])

    @staticmethod
    def test_update_time_checkout_without_build():
        """Test update_time_checkout_without_build method."""
        sql_metrics = metrics.DatawarehouseSQLMetrics()

        sql_metrics.metric_time_checkout_without_build = mock.Mock()
        sql_metrics.db_handler = mock.Mock()
        sql_metrics.db_handler.execute.return_value = [[1], [2], [None], [3]]

        sql_metrics.update_time_checkout_without_build()
        sql_metrics.metric_time_checkout_without_build.assert_has_calls([
            mock.call.observe(1),
            mock.call.observe(2),
            mock.call.observe(3),
        ])

    @staticmethod
    def test_update_psql_tables():
        """Test update_psql_tables method."""
        sql_metrics = metrics.DatawarehouseSQLMetrics()

        sql_metrics.metric_dw_table_size_rows = mock.Mock()
        sql_metrics.metric_dw_table_size_bytes = mock.Mock()
        sql_metrics.db_handler = mock.Mock()
        sql_metrics.db_handler.execute.return_value = [['table_foo', 1], ['table_bar', 2]]

        sql_metrics.update_psql_tables()
        sql_metrics.metric_dw_table_size_bytes.assert_has_calls([
            mock.call.labels('table_foo'),
            mock.call.labels().set(1),
            mock.call.labels('table_bar'),
            mock.call.labels().set(2)
        ])

        sql_metrics.metric_dw_table_size_rows.assert_has_calls([
            mock.call.labels('table_foo'),
            mock.call.labels().set(1),
            mock.call.labels('table_bar'),
            mock.call.labels().set(2)
        ])

    def test_update(self):
        """Test update calls all the methods."""
        sql_metrics = metrics.DatawarehouseSQLMetrics()

        sql_metrics.update_time_to_report = mock.Mock()
        sql_metrics.update_time_to_build = mock.Mock()
        sql_metrics.update_time_checkout_without_build = mock.Mock()
        sql_metrics.update_psql_tables = mock.Mock()

        sql_metrics.update()

        self.assertTrue(sql_metrics.update_time_to_report.called)
        self.assertTrue(sql_metrics.update_time_to_build.called)
        self.assertTrue(sql_metrics.update_time_checkout_without_build.called)
        self.assertTrue(sql_metrics.update_psql_tables.called)


class TestS3BucketMetrics(unittest.TestCase):
    """Test S3BucketMetrics methods."""

    @staticmethod
    @mock.patch.dict(
        os.environ, {
            'BUCKET_1': 'endpoint|access_key|secret_key|bucket|path',
            'BUCKET_2': 'endpoint|access_key|secret_key|bucket|path',
        }
    )
    @mock.patch('cki.cki_tools.service_metrics.metrics.s3buckets.S3_CONFIG', S3_CONFIG)
    @mock.patch('cki.cki_tools.service_metrics.metrics.s3buckets.get_bucket_size')
    @responses.activate
    def test_update(mock_get_bucket_size):
        """Test update method."""
        s3_metrics = metrics.S3BucketMetrics()

        mock_get_bucket_size.return_value = (1024, 2)
        s3_metrics.metric_bytes = mock.Mock()
        s3_metrics.metric_file_count = mock.Mock()

        s3_metrics.update()

        s3_metrics.metric_bytes.assert_has_calls([
            mock.call.labels('BUCKET_1', 'bucket 1'),
            mock.call.labels().set(1024),
            mock.call.labels('BUCKET_2', 'bucket 2'),
            mock.call.labels().set(1024)
        ])
        s3_metrics.metric_file_count.assert_has_calls([
            mock.call.labels('BUCKET_1', 'bucket 1'),
            mock.call.labels().set(2),
            mock.call.labels('BUCKET_2', 'bucket 2'),
            mock.call.labels().set(2)
        ])


class TestTEIIDBeakerMetrics(unittest.TestCase):
    """Test TEIIDBeakerMetrics methods."""

    def test_recipe_matches_queue(self):
        # pylint: disable=protected-access
        """Test _recipe_matches_queue method."""
        tbm = metrics.TEIIDBeakerMetrics()

        recipe_distro_required = """
            <distroRequires>
             <distro_arch op="=" value="i386"/>
             <distro_arch op="=" value="x86_64"/>
             <distro_family op="=" value="Fedora34"/>
             <distro_variant op="=" value="Server"/>
             <distro_name op="=" value="Fedora-34"/>
            </distroRequires>
        """
        self.assertFalse(tbm._recipe_matches_queue(recipe_distro_required, ['x86_64']))
        self.assertTrue(tbm._recipe_matches_queue(recipe_distro_required, ['x86_64', 'i386']))
        self.assertFalse(tbm._recipe_matches_queue(recipe_distro_required, ['i386']))
        self.assertFalse(tbm._recipe_matches_queue(recipe_distro_required, ['i386', 'aarch64']))

    @staticmethod
    @mock.patch('cki.cki_tools.service_metrics.metrics.teiidbeaker.BEAKER_CONFIG', BEAKER_CONFIG)
    @responses.activate
    def test_update_cki_beaker_recipes_in_queue():
        """Test update_cki_beaker_recipes_in_queue method."""
        tbm = metrics.TEIIDBeakerMetrics()
        tbm.metric_recipes_in_queue = mock.Mock()
        tbm.get_queued_recipes_constraints = mock.Mock()
        tbm.get_queued_recipes_constraints.return_value = [
            """<distroRequires>
             <distro_arch op="=" value="i386"/>
             <distro_arch op="=" value="x86_64"/>
            </distroRequires>""",
            """<distroRequires>
             <distro_arch op="=" value="x86_64"/>
            </distroRequires>""",
            """<distroRequires>
             <distro_arch op="=" value="aarch64"/>
            </distroRequires>""",
            """<distroRequires>
             <distro_arch op="=" value="x86_64"/>
            </distroRequires>""",
            """<distroRequires>
             <distro_arch op="=" value="ppc64"/>
            </distroRequires>""",
            """<distroRequires>
             <distro_arch op="=" value="ppc64le"/>
            </distroRequires>""",
        ]

        tbm.update_cki_beaker_recipes_in_queue()

        tbm.metric_recipes_in_queue.assert_has_calls([
            mock.call.labels('aarch64'),
            mock.call.labels().set(1),
            mock.call.labels('x86_64'),
            mock.call.labels().set(2),
            mock.call.labels('i386'),
            mock.call.labels().set(1),
            mock.call.labels('ppc64'),
            mock.call.labels().set(1),
            mock.call.labels('ppc64le'),
            mock.call.labels().set(1),
        ])

    def test_update(self):
        """Test update calls all the methods."""
        tbm = metrics.TEIIDBeakerMetrics()

        tbm.update_cki_beaker_recipes_in_queue = mock.Mock()

        tbm.update()

        self.assertTrue(tbm.update_cki_beaker_recipes_in_queue.called)


@mock.patch('cki.cki_tools.service_metrics.metrics.sentry.SENTRY_INSTANCES', SENTRY_INSTANCES)
@mock.patch.dict(os.environ, SENTRY_IO_TOKEN='some-token')
class TestSentryMetrics(unittest.TestCase):
    """Test SentryMetrics methods."""

    @responses.activate
    def test_update_project(self):
        """Test update_project method."""
        responses.add(
            responses.GET, 'https://sentry.io/api/0/teams/red-hat/cki/projects/',
            json=[{'slug': 'test-project'}]
        )
        responses.add(
            responses.GET, 'https://sentry.io/api/0/projects/red-hat/test-project/stats/',
            json=[[1.0, 123], ]
        )

        project = {'slug': 'test-project'}
        instance = SENTRY_INSTANCES[0]

        sentry_metrics = metrics.SentryMetrics()
        sentry_metrics.metric_sentry_events = mock.Mock()

        sentry_metrics.update_project(instance, project)

        self.assertEqual(
            sentry_metrics._previous_stat,  # pylint: disable=protected-access
            {'https://sentry.io': {'test-project': EventsCount(1.0, 123)}}
        )
        self.assertEqual(
            sentry_metrics._accumulated_value,  # pylint: disable=protected-access
            {'https://sentry.io': {'test-project': 123}}
        )
        sentry_metrics.metric_sentry_events.assert_has_calls([
            mock.call.labels('https://sentry.io', 'test-project'),
            mock.call.labels().set(123)
        ])

    @responses.activate
    def test_update_project_expected_values(self):
        """Test update_project method aggregates the data correctly."""
        responses.add(
            responses.GET, 'https://sentry.io/api/0/teams/red-hat/cki/projects/',
            json=[{'slug': 'test-project'}]
        )
        responses.add(
            responses.GET, 'https://sentry.io/api/0/projects/red-hat/test-project/stats/',
        )

        project = {'slug': 'test-project'}
        instance = SENTRY_INSTANCES[0]

        sentry_metrics = metrics.SentryMetrics()
        sentry_metrics.metric_sentry_events = mock.Mock()

        cases = [
            ([[1.0, 1], ], 1),
            ([[1.0, 2], ], 2),
            ([[1.0, 3], ], 3),
            ([[1.0, 3], [2.0, 0]], 3),  # 3 + 0
            ([[1.0, 3], [2.0, 2]], 5),  # 3 + 2
            ([[1.0, 3], [2.0, 5]], 8),  # 3 + 5
            ([[1.0, 3], [2.0, 5], [3.0, 1]], 9),  # 3 + 5 + 1
            # When old slices are not returned anymore that shouldnt affect.
            ([[3.0, 2], ], 10),  # 3 + 5 + 2
            ([[3.0, 2], [4.0, 5]], 15),  # 3 + 5 + 2 + 5
        ]

        for data, expected in cases:
            responses.replace(
                responses.GET, 'https://sentry.io/api/0/projects/red-hat/test-project/stats/',
                json=data,
            )
            sentry_metrics.update_project(instance, project)

            self.assertEqual(
                sentry_metrics._accumulated_value,  # pylint: disable=protected-access
                {'https://sentry.io': {'test-project': expected}}
            )

        sentry_metrics.metric_sentry_events.assert_has_calls([
            mock.call.labels('https://sentry.io', 'test-project'),
            mock.call.labels().set(1),
            mock.call.labels('https://sentry.io', 'test-project'),
            mock.call.labels().set(2),
            mock.call.labels('https://sentry.io', 'test-project'),
            mock.call.labels().set(3),
            mock.call.labels('https://sentry.io', 'test-project'),
            mock.call.labels().set(3),
            mock.call.labels('https://sentry.io', 'test-project'),
            mock.call.labels().set(5),
            mock.call.labels('https://sentry.io', 'test-project'),
            mock.call.labels().set(8),
            mock.call.labels('https://sentry.io', 'test-project'),
            mock.call.labels().set(9),
            mock.call.labels('https://sentry.io', 'test-project'),
            mock.call.labels().set(10),
            mock.call.labels('https://sentry.io', 'test-project'),
            mock.call.labels().set(15)
        ])

    def test_update(self):
        """Test update call."""
        sentry_metrics = metrics.SentryMetrics()
        # pylint: disable=protected-access
        sentry_metrics._apis['https://sentry.io'].projects = [
            {'slug': 'proj-1'}, {'slug': 'proj-2'}, {'slug': 'proj-3'},
        ]
        sentry_metrics.update_project = mock.Mock()

        sentry_metrics.update()
        sentry_metrics.update_project.assert_has_calls([
            mock.call(SENTRY_INSTANCES[0], {'slug': 'proj-1'}),
            mock.call(SENTRY_INSTANCES[0], {'slug': 'proj-2'}),
            mock.call(SENTRY_INSTANCES[0], {'slug': 'proj-3'}),
        ])


@mock.patch.dict(os.environ, SENTRY_IO_TOKEN='some-token')
class TestSentryAPI(unittest.TestCase):
    """Test SentryAPI methods."""

    @responses.activate
    def test_get(self):
        """Test _get method."""
        responses.add(
            responses.GET, 'https://sentry.io/foo/red-hat/cki', json={}
        )
        api = SentryAPI(SENTRY_INSTANCES[0])
        api._get('/foo/{org}/{team}')  # pylint: disable=protected-access

        self.assertEqual(
            responses.calls[0].request.url,
            'https://sentry.io/foo/red-hat/cki'
        )
        self.assertEqual(
            responses.calls[0].request.headers['Authorization'],
            'Bearer some-token'
        )

    def test_projects(self):
        # pylint: disable=protected-access
        """Test projects property."""
        api = SentryAPI(SENTRY_INSTANCES[0])
        api._get = mock.Mock()

        api.projects  # pylint: disable=pointless-statement

        api._get.assert_called_with('/api/0/teams/{org}/{team}/projects/')

    def test_project_stats(self):
        # pylint: disable=protected-access
        """Test get_project_stats method."""
        api = SentryAPI(SENTRY_INSTANCES[0])
        api._get = mock.Mock()

        proj = {'slug': 'proj-1', 'foo': 'bar'}
        api.get_project_stats(proj)

        api._get.assert_called_with('/api/0/projects/{org}/proj-1/stats/')


@mock.patch('cki_lib.kubernetes.KubernetesHelper.setup', mock.Mock())
class TestKubernetesMetrics(unittest.TestCase):
    """Test KubernetesMetrics methods."""

    @URLLIB3_MOCK.activate
    def test_update_cluster(self):
        # pylint: disable=protected-access
        """Test update_cluster method."""
        body = {
            'items': [
                {'metadata': {'name': 'some-pod-55-8fgnw'},
                 'containers': [
                    {'name': 'promtail-sidecar', 'usage': {'cpu': '5m', 'memory': '46696Ki'}},
                    {'name': 'default', 'usage': {'cpu': '2m', 'memory': '70392Ki'}}
                ]},
                {'metadata': {'name': 'another-pod-5-aaaaa'},
                 'containers': [
                    {'name': 'promtail-sidecar', 'usage': {'cpu': '1025m', 'memory': '4Mi'}},
                    {'name': 'default', 'usage': {'cpu': '0', 'memory': '0'}}
                ]},
            ]
        }

        URLLIB3_MOCK.add(
            'GET', '/apis/metrics.k8s.io/v1beta1/namespaces/cki/pods',
            body=json.dumps(body), status=200, content_type='application/json'
        )

        k8s_metrics = metrics.KubernetesMetrics()
        k8s_metrics.k8s.namespace = 'cki'
        k8s_metrics.metric_usage = mock.Mock()
        k8s_metrics._delete_missing_containers = mock.Mock()

        k8s_metrics.update_cluster()

        k8s_metrics.metric_usage.assert_has_calls([
            mock.call.labels('some-pod-55-8fgnw', 'promtail-sidecar', 'memory'),
            mock.call.labels().set(47816704.0),
            mock.call.labels('some-pod-55-8fgnw', 'promtail-sidecar', 'cpu'),
            mock.call.labels().set(0.005),
            mock.call.labels('some-pod-55-8fgnw', 'default', 'memory'),
            mock.call.labels().set(72081408.0),
            mock.call.labels('some-pod-55-8fgnw', 'default', 'cpu'),
            mock.call.labels().set(0.002),
            mock.call.labels('another-pod-5-aaaaa', 'promtail-sidecar', 'memory'),
            mock.call.labels().set(4194304.0),
            mock.call.labels('another-pod-5-aaaaa', 'promtail-sidecar', 'cpu'),
            mock.call.labels().set(1.025),
            mock.call.labels('another-pod-5-aaaaa', 'default', 'memory'),
            mock.call.labels().set(0.0),
            mock.call.labels('another-pod-5-aaaaa', 'default', 'cpu'),
            mock.call.labels().set(0.0),
        ])
        k8s_metrics._delete_missing_containers.assert_called_with(body['items'])

    def test_update(self):
        """Test update calls update_cluster correctly."""
        k8s_metrics = metrics.KubernetesMetrics()
        k8s_metrics.update_cluster = mock.Mock()

        k8s_metrics.update()

        self.assertTrue(k8s_metrics.update_cluster.called)

    @URLLIB3_MOCK.activate
    def test_delete_missing_containers(self):
        # pylint: disable=protected-access
        """Test _delete_missing_containers method."""
        body = {
            'items': [
                {'metadata': {'name': 'some-pod-55-8fgnw'},
                 'containers': [
                    {'name': 'promtail-sidecar', 'usage': {'cpu': '5m', 'memory': '46696Ki'}},
                    {'name': 'default', 'usage': {'cpu': '2m', 'memory': '70392Ki'}}
                ]},
                {'metadata': {'name': 'another-pod-5-aaaaa'},
                 'containers': [
                    {'name': 'promtail-sidecar', 'usage': {'cpu': '1025m', 'memory': '4Mi'}},
                    {'name': 'default', 'usage': {'cpu': '0', 'memory': '0'}}
                ]},
            ]
        }

        k8s_metrics = metrics.KubernetesMetrics()
        k8s_metrics.k8s.namespace = 'cki'
        k8s_metrics.metric_usage.remove = mock.Mock()

        # Update with all the pods
        URLLIB3_MOCK.add(
            'GET', '/apis/metrics.k8s.io/v1beta1/namespaces/cki/pods',
            body=json.dumps(body), status=200, content_type='application/json'
        )
        k8s_metrics.update_cluster()

        # Delete one pod from the response and call update again
        del body['items'][1]
        URLLIB3_MOCK.reset()
        URLLIB3_MOCK.add(
            'GET', '/apis/metrics.k8s.io/v1beta1/namespaces/cki/pods',
            body=json.dumps(body), status=200, content_type='application/json'
        )
        k8s_metrics.update_cluster()

        # Missing pod is deleted from labels
        k8s_metrics.metric_usage.remove.assert_has_calls([
            mock.call('another-pod-5-aaaaa', 'default', 'memory'),
            mock.call('another-pod-5-aaaaa', 'default', 'cpu'),
            mock.call('another-pod-5-aaaaa', 'promtail-sidecar', 'memory'),
            mock.call('another-pod-5-aaaaa', 'promtail-sidecar', 'cpu')
        ], any_order=True)


class TestVolumeMetrics(unittest.TestCase):
    """Test VolumeMetrics methods."""

    @staticmethod
    @mock.patch('cki.cki_tools.service_metrics.metrics.volume.shutil.disk_usage')
    @mock.patch('cki.cki_tools.service_metrics.metrics.volume.VOLUMES_LIST',
                [{'name': 'vol_1', 'path': '/path_1'}, {'name': 'vol_2', 'path': '/path_2'}])
    def test_update(mock_disk_usage):
        """Test update_pool_count method."""
        usage = mock.Mock()
        usage.used = 123
        mock_disk_usage.return_value = usage

        volume_metrics = metrics.VolumeMetrics()
        volume_metrics.metric_usage = mock.Mock()

        volume_metrics.update()

        mock_disk_usage.assert_has_calls([
            mock.call('/path_1'),
            mock.call('/path_2'),
        ])

        volume_metrics.metric_usage.assert_has_calls([
            mock.call.labels('vol_1'),
            mock.call.labels().set(123),
            mock.call.labels('vol_2'),
            mock.call.labels().set(123)
        ])


class TestAwsCostMetrics(unittest.TestCase):
    """Test AwsCostMetrics methods."""

    def _get_cost_and_usage(self, GroupBy=None, *args, **kwargs):
        if GroupBy[0]['Key'] == 'ServiceComponent':
            return {
                'ResultsByTime': [{
                    'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                    'Groups': [{
                        'Keys': ['ServiceComponent$Build'],
                        'Metrics': {'BlendedCost': {'Amount': '1', 'Unit': 'USD'}}
                    }, {
                        'Keys': ['ServiceComponent$DomainNames'],
                        'Metrics': {'BlendedCost': {'Amount': '2', 'Unit': 'USD'}}
                    }]
                }]}
        if GroupBy[0]['Key'] == 'ServiceOwner':
            return {
                'ResultsByTime': [{
                    'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                    'Groups': [{
                        'Keys': ['ServiceOwner$CkiProject'],
                        'Metrics': {'BlendedCost': {'Amount': '3', 'Unit': 'USD'}}
                    }]
                }]}
        if GroupBy[0]['Key'] == 'SERVICE':
            return {
                'ResultsByTime': [{
                    'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                    'Groups': [{
                        'Keys': ['AWS Lambda'],
                        'Metrics': {'BlendedCost': {'Amount': '4', 'Unit': 'USD'}}
                    }, {
                        'Keys': ['EC2 - Other'],
                        'Metrics': {'BlendedCost': {'Amount': '5', 'Unit': 'USD'}}
                    }]
                }]}
        if GroupBy[0]['Key'] == 'USAGE_TYPE':
            return {'ResultsByTime': [{
                'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                'Groups': [{
                    'Keys': ['BoxUsage:t3a.medium'],
                    'Metrics': {'BlendedCost': {'Amount': '6', 'Unit': 'USD'}}
                }, {
                    'Keys': ['BoxUsage:t3a.micro'],
                    'Metrics': {'BlendedCost': {'Amount': '7', 'Unit': 'USD'}}
                }]
            }]}
        if GroupBy[0]['Key'] == 'INSTANCE_TYPE':
            return {'ResultsByTime': [{
                'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                'Groups': [{
                    'Keys': ['NoInstanceType'],
                    'Metrics': {'BlendedCost': {'Amount': '8', 'Unit': 'USD'}}
                }, {
                    'Keys': ['c5d.4xlarge'],
                    'Metrics': {'BlendedCost': {'Amount': '9', 'Unit': 'USD'}}
                }]
            }]}
        if GroupBy[0]['Key'] == 'OPERATION':
            return {'ResultsByTime': [{
                'TimePeriod': {'Start': '2021-07-15', 'End': '2021-07-16'},
                'Groups': [{
                    'Keys': ['A'],
                    'Metrics': {'BlendedCost': {'Amount': '10', 'Unit': 'USD'}}
                }, {
                    'Keys': ['AAAA'],
                    'Metrics': {'BlendedCost': {'Amount': '11', 'Unit': 'USD'}}
                }]
            }]}

    @mock.patch('boto3.Session')
    def test_update(self, session):
        """Test aws cost metrics."""
        cost_metrics = metrics.AwsCostMetrics()
        cost_metrics.metric_cost = mock.Mock()

        session().client().get_cost_and_usage.side_effect = self._get_cost_and_usage
        cost_metrics.update()

        cost_metrics.metric_cost.assert_has_calls([
            mock.call.labels('component', 'Build').set(1.0),
            mock.call.labels('component', 'DomainNames').set(2.0),
            mock.call.labels('owner', 'CkiProject').set(3.0),
            mock.call.labels('service', 'AWS Lambda').set(4.0),
            mock.call.labels('service', 'EC2-Other').set(5.0),
            mock.call.labels('usage_type', 't3a.medium').set(6.0),
            mock.call.labels('usage_type', 't3a.micro').set(7.0),
            mock.call.labels('instance_type', 'NoInstanceType').set(8.0),
            mock.call.labels('instance_type', 'c5d.4xlarge').set(9.0),
            mock.call.labels('operation', 'A').set(10.0),
            mock.call.labels('operation', 'AAAA').set(11.0),
        ], any_order=True)
