"""YAML CLI tool tests."""
import contextlib
import io
import sys
import typing
import unittest

import yaml

from cki.cki_tools import yaml as cki_yaml


class TestYaml(unittest.TestCase):
    """Test YAML CLI tool."""

    @staticmethod
    @contextlib.contextmanager
    def redirect(what: str, new: typing.IO[str]) -> typing.Iterator[typing.IO[str]]:
        """Redirect a stream."""
        old = getattr(sys, what)
        setattr(sys, what, new)
        try:
            yield new
        finally:
            setattr(sys, what, old)

    def test_yaml_set_value(self):
        """Ensure rc_set works."""
        with self.redirect('stdout', io.StringIO()) as stdout, \
                self.redirect('stdin', io.StringIO('foo: [qux, {bar: baz}]')):
            cki_yaml.main(['set-value', 'foo.1.bar', 'zig'])
        self.assertEqual(yaml.safe_load(stdout.getvalue()),
                         {'foo': ['qux', {'bar': 'zig'}]})

    def test_yaml_set_value_last_array(self):
        """Ensure rc_set works with array elements."""
        with self.redirect('stdout', io.StringIO()) as stdout, \
                self.redirect('stdin', io.StringIO('foo: [bar, baz]')):
            cki_yaml.main(['set-value', 'foo.1', 'zig'])
        self.assertEqual(yaml.safe_load(stdout.getvalue()),
                         {'foo': ['bar', 'zig']})
